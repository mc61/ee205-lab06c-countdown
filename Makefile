###############################################################################
#         University of Hawaii, College of Engineering
# @brief  Lab 04c - Countdown - EE 205 - Spr 2022
#
# @file    Makefile
# @version 1.0
#
# @author Caleb Mueller <mc61@hawaii.edu>
# @date   20_FEB_2020
#
# @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

TARGET = countdown


all:  $(TARGET)


CC     = g++
CFLAGS = -Wall -Wextra $(DEBUG_FLAGS)


debug: DEBUG_FLAGS = -g -DDEBUG
debug: clean $(TARGET)


countdown.o: countdown.cpp
	$(CC) $(CFLAGS) -c countdown.cpp


countdown: countdown.o
	$(CC) $(CFLAGS) -o $(TARGET) countdown.o


test: $(TARGET)
	./$(TARGET)


clean:
	rm -f $(TARGET) *.o
