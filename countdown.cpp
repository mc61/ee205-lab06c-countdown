///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   $ ./countdown
// Results in countdown timer counting to/from reference date
//   Reference time: Tue Jan 21 04:26:07 PM HST 2014
//   Years: 7 Days: 12 Hours: 6 Minutes: 20 Seconds: 57
//   Years: 7 Days: 12 Hours: 6 Minutes: 20 Seconds: 58
//
// @author Caleb Mueller<mc61@hawaii.edu>
// @date   20_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#include <time.h>
#include <locale.h>

#include <iostream>
#include <chrono>
#include <thread>

using namespace std;

void printCountDown(time_t);

int main()
{

   // Creating the reference time tm struct
   // How long until graduation
   time_t refRawtime;
   struct tm refTimeinfo;
   refTimeinfo = *localtime(&refRawtime);

   // Assigning the reference date to the struct
   refTimeinfo.tm_year = 2023 - 1900;
   refTimeinfo.tm_mon = 12 - 1;
   refTimeinfo.tm_mday = 16;
   refTimeinfo.tm_hour = 0;
   refTimeinfo.tm_min = 0;
   refTimeinfo.tm_sec = 0;
   refTimeinfo.tm_isdst = -1;

   char referenceBuffer[80];

   // Printing the reference time in the specified format
   strftime(referenceBuffer, 80, "Reference time: %a %b %d %I:%M:%S %p %Z %Y", &refTimeinfo);
   cout << referenceBuffer << endl;

   while (true)
   {
      //  Updating currentRawTime to the present
      time_t currentRawTime;
      time(&currentRawTime);

      time_t deltaSeconds = difftime(currentRawTime, mktime(&refTimeinfo));
      printCountDown(deltaSeconds);

      std::this_thread::sleep_for(std::chrono::seconds(1));
   }
   return 0;
}

void printCountDown(const time_t secs)
{
   long int deltaSecs = abs(secs);

   // Conversion Constants
   const long int S_IN_Y = 31540000;
   const long int S_IN_D = 86400;
   const long int S_IN_H = 3600;
   const long int S_IN_M = 60;

   int dYears = deltaSecs / S_IN_Y;
   int dDays = (deltaSecs - (dYears * S_IN_Y)) / S_IN_D;
   int dHours = (deltaSecs - (dYears * S_IN_Y) - (dDays * S_IN_D)) / S_IN_H;
   int dMinutes = (deltaSecs - (dYears * S_IN_Y) - (dDays * S_IN_D) - (dHours * S_IN_H)) / S_IN_H;
   int dSeconds = (deltaSecs % S_IN_M);

   cout << "Years: " << dYears << " Days: " << dDays << " Hours: " << dHours;
   cout << " Minutes: " << dMinutes << " Seconds: " << dSeconds << endl;
}
